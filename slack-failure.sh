#!/bin/bash

# Construct the JSON payload for the Slack message
payload=$(
  cat <<EOF
{
  "text": "Pipeline *failed* for project: ${CI_PROJECT_PATH}\nBranch: ${CI_COMMIT_REF_NAME}\nJob: ${CI_JOB_NAME}\n<${CI_JOB_URL}|View Job>",
  "username": "gitlab-ci",
  "icon_emoji": ":red_circle:"
}
EOF
)

# Send the payload to the Slack webhook URL
curl -X POST -H 'Content-type: application/json' --data "$payload" $SLACK_WEBHOOK_URL

