#!/bin/bash

# Set variables
GITLAB_URL="https://gitlab.com/api/v4"
PROJECT_ID="59065447"
PRIVATE_TOKEN="glpat-g_NsJFbBVy5GZYmMhaAK"

BRANCH_NAME=$CI_COMMIT_REF_NAME
COMMIT_HASH=$CI_COMMIT_SHA
BUILD_URL=$CI_PIPELINE_URL

# Create issue JSON payload
ISSUE_PAYLOAD=$(cat <<EOF
{
  "title": "Build Pipeline Failed on branch $BRANCH_NAME",
  "description": "The build pipeline failed for commit $COMMIT_HASH. Please check the logs for more details.\n\nBuild details:\n- Branch: $BRANCH_NAME\n- Commit: $COMMIT_HASH\n- Build URL: $BUILD_URL",
  "labels": "issue::production-bug,auto-created"
}
EOF
)

# Send POST request to GitLab API to create the issue
curl --request POST "$GITLAB_URL/projects/$PROJECT_ID/issues" \
     --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --header "Content-Type: application/json" \
     --data "$ISSUE_PAYLOAD"
